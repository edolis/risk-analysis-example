# README #

**Quick risk-analysis of 141 real patients with cardiovascular diseases.**

* Two models: Cow Proportional Hazard and GLM (Logistic Regression).
* Both models try to quantify the effect of 27 potential risk factors
on 1000-days survival of patients.
* The response contains data on whether (and when) each patient dies.
* In both models, first we perform a LASSO analysis to do variable selection.
* Both models end with a prediction analysis via ROC curve and AUC.

Contains three files:
1. testCOX.R: Cox model analysis performed in R.
2. testGLM.R: GLM (Logistic) analysis performed in R.
3. GLMroc.ipynb: Same GLM analysis, but on ipython notebook, using scikit.